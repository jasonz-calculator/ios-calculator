import UIKit

struct Settings {
    var enableLargeFont: Bool = false
    var enableRoundButton: Bool = false
    var enableLog: Bool = false
        
    enum Keys: String {
        case enableLargeFont
        case enableRoundButton
        case enableLog
    }
    
    init() {
        load()
    }
    
    mutating func load() {
        enableLargeFont = load(Keys.enableLargeFont.rawValue, defaultValue: false)
        enableRoundButton = load(Keys.enableRoundButton.rawValue, defaultValue: false)
        enableLog = load(Keys.enableLog.rawValue, defaultValue: false)
    }
    
    mutating func save() {
        UserDefaults.standard.set(enableLargeFont, forKey: Keys.enableLargeFont.rawValue)
        UserDefaults.standard.set(enableRoundButton, forKey: Keys.enableRoundButton.rawValue)
        UserDefaults.standard.set(enableLog, forKey: Keys.enableLog.rawValue)
    }
    
    mutating func load(_ key: String, defaultValue: Bool) -> Bool {
        if let obj = UserDefaults.standard.object(forKey: key) {
            return obj as! Bool
        }
        else {
            return defaultValue
        }
    }
}
