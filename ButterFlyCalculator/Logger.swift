import Foundation

class Logger {
    private static var logInited = false
    private static var logFileName = "bfcalc-log.txt"
    
    static func isLogFileExist() -> Bool {
        var ret = false
        
        if let cacheFolder = FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask).first {
            let logFilePath = cacheFolder.appendingPathComponent(logFileName)
            ret = FileManager.default.fileExists(atPath: logFilePath.path)
        }
        
        return ret
    }
    
    static func logFileUrl() -> URL? {
        var ret: URL? = nil
        if let cacheFolder = FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask).first {
            ret = cacheFolder.appendingPathComponent(logFileName)
        }
        return ret
    }
    
    static func log(_ info: String) {
        if Settings().enableLog {
            if !logInited {
                if let cacheFolder = FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask).first {
                    let logFilePath = cacheFolder.appendingPathComponent(logFileName)
                    
                    // Redirect stderr to file so that we can catch every info
                    freopen(logFilePath.path.cString(using: .utf8)!, "a+", stderr)
                    logInited = true
                }
            }
            
            //
            if logInited {
                NSLog(info)
            }
        }
    }
}
