import UIKit

class MainViewController: UIViewController {
    
    @IBOutlet var buttons: [UIButton]!
    @IBOutlet var settingButton: UIButton!
    @IBOutlet var resultLabel: UILabel!
    @IBOutlet var resultLabelHeightConstraint: NSLayoutConstraint!
    
    private let calcEngine = CalculatorEngine();
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initAllButtons()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        initAllButtons()
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        resultLabelHeightConstraint.constant = UIDevice.current.orientation == .portrait ? 200 : 100
    }
    
    private func initAllButtons() {
        let settings = Settings();
        
        //
        let borderWidth: CGFloat = 0.3
        let borderColor = UIColor.lightGray.cgColor
        let fontSize: CGFloat = settings.enableLargeFont ? 40 : 20
        let cornelRadius: CGFloat = settings.enableRoundButton ? 20 : 0
        
        //
        settingButton.setTitle("\u{2699}", for: .normal) //Gear icon
        settingButton.layer.borderWidth = borderWidth
        settingButton.layer.borderColor = borderColor
        settingButton.layer.cornerRadius = cornelRadius

        //
        for button in self.buttons {
            button.layer.borderWidth = borderWidth
            button.layer.borderColor = borderColor
            button.layer.cornerRadius = cornelRadius
            button.titleLabel?.font = UIFont.boldSystemFont(ofSize: fontSize)

            //
            button.addTarget(self, action: #selector(onButtonTouched(button:)),for: .touchUpInside)
        }
    }
    
    @objc private func onButtonTouched(button: UIButton) {
        if let operand = button.currentTitle {
            if operand == "C" {
                resultLabel.text = "0"
                calcEngine.reset()
            } else {
                resultLabel.text = calcEngine.add(operand)
            }
        }
    }    
}

