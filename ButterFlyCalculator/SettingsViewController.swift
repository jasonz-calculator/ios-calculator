import UIKit

class SettingsViewController: UITableViewController, UIDocumentInteractionControllerDelegate {
    private enum Section: Int {
        case VERSION = 0, APPEARANCE, TROUBLESHOOTING
    }
        
    private enum VersionSection: Int {
        case BUILDNUM = 0, COPYRIGHT
    }

    private enum AppearanceSection: Int {
        case LARGEFONT = 0, ROUNDBUTTONS
    }
    
    private enum TroubleShootingSection: Int {
        case ENABLELOG = 0, SHARELOG, VIEWLOG
    }

    private let SECTION_TITLES = [
        "Settings-SectionTitle-Version",
        "Settings-SectionTitle-Appearance",
        "Settings-SectionTitle-TroubleShooting"
    ]
    
    private let SECTION_ROW_TITLES = [
        0: ["Settings-Version-BuildNumber", "Settings-Version-Copyright"],
        1: ["Settings-Appearance-LargeFont", "Settings-Appearance-RoundButtons"],
        2: ["Settings-TroubleShooting-EnableLog", "Settings-TroubleShooting-ShareLog", "Settings-TroubleShooting-ViewLog"]
    ]
    
    private let CELL_ID = "settings-cell-id"
    private let COPYRIGHT_TEXT = "©2020"
    
    //
    private var largeFontSwitch = UISwitch(frame: .zero)
    private var roundButtonSwitch = UISwitch(frame: .zero)
    private var enableLogSwitch = UISwitch(frame: .zero)
    private var shareLogCell = UITableViewCell(style: .default, reuseIdentifier: nil)
    private var viewLogCell = UITableViewCell(style: .default, reuseIdentifier: nil)

    //
    private var settings = Settings()

    //
    override func viewDidLoad() {
        super.viewDidLoad()

        //
        let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(onDone(_:)))
        self.navigationItem.setRightBarButton(doneButton, animated: false)
        self.title = NSLocalizedString("Settings-Title", comment: "")
        
        //
        largeFontSwitch.isOn = settings.enableLargeFont
        roundButtonSwitch.isOn = settings.enableRoundButton
        enableLogSwitch.isOn = settings.enableLog
        enableLogSwitch.addTarget(self, action: #selector(onEnableLogSwitchChanged), for: .valueChanged)

        shareLogCell.textLabel?.isEnabled = settings.enableLog && Logger.isLogFileExist()
        shareLogCell.accessoryType = .disclosureIndicator
        shareLogCell.selectionStyle = .none
        
        viewLogCell.textLabel?.isEnabled = settings.enableLog && Logger.isLogFileExist()
        viewLogCell.accessoryType = .disclosureIndicator
        viewLogCell.selectionStyle = .none
    }
    
    @objc func onDone(_ sender: Any) {
        settings.enableLargeFont = largeFontSwitch.isOn
        settings.enableRoundButton = roundButtonSwitch.isOn
        settings.enableLog = enableLogSwitch.isOn
        settings.save()
        
        self.dismiss(animated: true, completion: nil)
    }
        
    @objc func onEnableLogSwitchChanged(switchControl: UISwitch) {
        shareLogCell.textLabel?.isEnabled = switchControl.isOn && Logger.isLogFileExist()
        viewLogCell.textLabel?.isEnabled = switchControl.isOn && Logger.isLogFileExist()

    }
    
    // MARK: Table delegates
    override func numberOfSections(in tableView: UITableView) -> Int {
        return SECTION_TITLES.count
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return NSLocalizedString(SECTION_TITLES[section], comment: "")
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let rowTitles = SECTION_ROW_TITLES[section] {
            return rowTitles.count
        } else {
            return 0
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var reusableCell = tableView.dequeueReusableCell(withIdentifier: CELL_ID)
        
        if reusableCell == nil || reusableCell?.detailTextLabel == nil {
            reusableCell = UITableViewCell(style: .value1, reuseIdentifier: CELL_ID)
        }
        
        var cell = reusableCell!
        
        cell.selectionStyle = .none

        // Set cell title
        if let rowTitles = SECTION_ROW_TITLES[indexPath.section] {
            if indexPath.row < rowTitles.count {
                cell.textLabel?.text = NSLocalizedString(rowTitles[indexPath.row], comment: "")
            }
        }

        // Set cell accessory view
        switch indexPath.section {
            case Section.VERSION.rawValue:
                switch indexPath.row {
                    case VersionSection.BUILDNUM.rawValue:
                        cell.detailTextLabel?.text = Bundle.main.infoDictionary?["CFBundleVersion"] as? String
                    case VersionSection.COPYRIGHT.rawValue:
                        cell.detailTextLabel?.text = COPYRIGHT_TEXT
                    default:
                        break
                }

            case Section.APPEARANCE.rawValue:
                switch indexPath.row {
                    case AppearanceSection.LARGEFONT.rawValue:                        
                        cell.accessoryView = largeFontSwitch
                    case AppearanceSection.ROUNDBUTTONS.rawValue:
                        cell.accessoryView = roundButtonSwitch
                    default:
                        break
                }
                
            case Section.TROUBLESHOOTING.rawValue:
                switch indexPath.row {
                    case TroubleShootingSection.ENABLELOG.rawValue:
                        cell.accessoryView = enableLogSwitch
                    case TroubleShootingSection.SHARELOG.rawValue:
                        shareLogCell.textLabel?.text = cell.textLabel?.text
                        cell = shareLogCell
                    case TroubleShootingSection.VIEWLOG.rawValue:
                        viewLogCell.textLabel?.text = cell.textLabel?.text
                        cell = viewLogCell
                    default:
                        break
                }
                
            default:
                break
        }
        
        return cell;
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == Section.TROUBLESHOOTING.rawValue {
            if Logger.isLogFileExist() && enableLogSwitch.isOn {
                if let logFileUrl = Logger.logFileUrl() {
                    let docInteractCtrl = UIDocumentInteractionController(url: logFileUrl)
                    docInteractCtrl.delegate = self
                    
                    if indexPath.row == TroubleShootingSection.SHARELOG.rawValue {
                        docInteractCtrl.presentOptionsMenu(from: .zero, in: self.tableView, animated: true)
                    } else if indexPath.row == TroubleShootingSection.VIEWLOG.rawValue {
                        docInteractCtrl.presentPreview(animated: true)
                    }
                }
            }
        }
    }
    
    // MARK: Document Interaction delegates
    func documentInteractionControllerViewControllerForPreview(_ controller: UIDocumentInteractionController) -> UIViewController {
        return self
    }
}
