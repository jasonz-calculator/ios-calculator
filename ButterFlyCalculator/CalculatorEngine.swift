import Foundation

class CalculatorEngine {
    let ERROR_RESULT = NSLocalizedString("CalculatorEngine-Error", comment: "")
    
    var operatorStack: [String] = []
    var digitStack: [Double] = []
    
    var displayText = ""
    var result: Double? = 0
    var isFloatNum = false
    var isNegtive = false
    var isNewText = false
    var lastDigit: Double?
    var lastOperator: String?
    var inputing = false;
    
    //
    func add(_ item: String) -> String {
        let operand = item.trimmingCharacters(in: .whitespacesAndNewlines)
        
        if isNewText {
            displayText = ""
            isNegtive = false
            isFloatNum = false
            isNewText = false
        }
        
        //
        switch operand {
            case "0"..."9":
                if displayText.isEmpty {
                    displayText.append(operand)
                } else {
                    let first = displayText.first!

                    if first == "0" && !isFloatNum {
                        displayText = ""
                    }
                    
                    displayText.append(operand)
                }
                lastDigit = nil
                inputing = true
            
            case "+", "-", "*", "/":
                lastOperator = operand

                if inputing {
                    // Operate with new inputd digits
                    inputing = false
                    lastDigit = Double(displayText)
                    if lastDigit != nil {
                        digitStack.append(lastDigit!)
                        result = updateStack(op: operand)
                        operatorStack.append(operand)
                    } else {
                        displayText = ERROR_RESULT
                        break
                    }
                } else if digitStack.isEmpty && operatorStack.isEmpty && result != nil{
                    // Operate with the result of last time
                    digitStack.append(result!)
                    operatorStack.append(operand)
                } else if !operatorStack.isEmpty {
                    // Replace last time inputed operator
                    operatorStack.removeLast()
                    result = updateStack(op: operand)
                    operatorStack.append(operand)
                }
                
                displayText = toResultText(result)
                isNewText = true

            case "=":
                if inputing {
                    inputing = false
                    lastDigit = Double(displayText)
                } else if lastDigit == nil {
                    break
                }
                
                // When click "=" key multiple times
                if operatorStack.isEmpty {
                    if let op = lastOperator, let _result = result {
                        digitStack.append(_result)
                        operatorStack.append(op)
                    }
                }
                
                digitStack.append(lastDigit!)
                result = updateStack(op: operand)
                displayText = toResultText(result)
        
                digitStack.removeAll()
                isNewText = true
                
            case ".":
                if !isFloatNum {
                    isFloatNum = true
                    displayText.isEmpty ? displayText.append("0.") : displayText.append(".")
                }
            
            case "+/-":
                if inputing && displayText.count > 0 {
                    if isNegtive {
                        displayText.remove(at: displayText.startIndex)
                    } else {
                        displayText.insert("-", at: displayText.startIndex)
                    }
                    isNegtive = !isNegtive
                } else if result != nil{
                    result = -(result!)
                    displayText = toResultText(result)
                    inputing = true
                }

            default:
                displayText = ERROR_RESULT
        }
        
        return displayText
    }
    
    func reset() {
        digitStack = []
        operatorStack = []
        isNegtive = false
        isFloatNum = false
        displayText = ""
        lastDigit = nil
        lastOperator = nil
        result = 0
    }
    
    private func toResultText(_ result: Double?) -> String {
        if let ret = result {
            if ret < Double(Int64.max) && ret > Double(Int64.min) {
                // If decimal part is 0, display it as Int
                let intValue = Int64(ret);
                if ret > Double(intValue) {
                    return String(ret)
                } else {
                    return String(intValue)
                }

            } else {
                return String(ret)
            }
        } else {
            return ERROR_RESULT
        }
    }
    
    private func updateStack(op: String) -> Double? {
        var doCalc = false
        
        repeat {
            doCalc = false
            
            if op == "+" || op == "-" {
                doCalc = true
            } else if op == "*" || op == "/" {
                if let top = operatorStack.last {
                    doCalc = top == "*" || top == "/"
                }
            } else {
                doCalc = true
            }

            //
            doCalc = doCalc && !operatorStack.isEmpty && digitStack.count >= 2
            
            if doCalc {
                let right = digitStack.removeLast()
                let left = digitStack.removeLast()
                let op = operatorStack.removeLast()
                
                Logger.log("\(left) \(op) \(right)")

                switch op {
                    case "+":
                        digitStack.append(left + right)
                    case "-":
                        digitStack.append(left - right)
                    case "*":
                        digitStack.append(left * right)
                    case "/":
                        if right == 0 {
                            return nil
                        }
                        digitStack.append(left / right)
                    default:
                        break
                }
            }
        } while (doCalc)
        
        //
        return digitStack.last ?? 0
    }
}
